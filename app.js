const https = require('https');

https.get('https://coderbyte.com/api/challenges/json/json-cleaning', (resp) => {

  let content = '';

 // push data character by character to "content" variable

 resp.on('data', (c) => content += c);

 // when finished reading all data, parse it for what we need now

 resp.on('end', () => {

   let jsonContent = JSON.parse(content);
    console.log('Prev Object', jsonContent)
    const cleanObj = (obj) => {
    let newObject = {};
    Object.keys(obj).forEach((key) => {
        // console.log('KEY',key)
        if (obj[key] === Object(obj[key])){ 
            newObject[key] = cleanObj(obj[key]);
        } else if (obj[key] != undefined && obj[key] != '' && obj[key] != '-' && obj[key] != 'N/A' && obj[key] != null) { // check the value
            newObject[key] = obj[key];
        }

        
    });
        return newObject;
    };

    let newObjectArr = cleanObj(jsonContent);
    console.log('New Object', newObjectArr);

 });

});